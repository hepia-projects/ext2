# -*- coding: utf-8 -*-
import hexdump
import stat
import struct
import math
import fs


class ext2_file_api(object):
    # 512 data blocks as per http://www.nongnu.org/ext2-doc/ext2.html#I-BLOCKS
    BLOCK_DATA_SIZE = 512
    INDIRECT_LINK_FROM_SIZE = 60

    openfiles = []

    def __init__(self, filesystem):
        self.fs = filesystem

    # For all symlink shorter than 60 bytes long, the data is stored within the inode itself;
    # it uses the fields which would normally be used to store the pointers to data blocks.
    # This is a worthwhile optimisation as it we avoid allocating a full block for the symlink,
    # and most symlinks are less than 60 characters long.
    def readlink(self, path):
        inode_index = self.fs.namei(path)
        inode = self.fs.inodes_list[inode_index]
        if inode.i_size < self.INDIRECT_LINK_FROM_SIZE:
            return ''.join([struct.pack("<I", inode.i_blocks[i]) for i in range(0, int(math.ceil(float(inode.i_size + 1) / 4)))])[:inode.i_size]

        # handle non indirect links
        return self.__read(inode, 0, inode.i_size)

    # open a file, i.e reserve a file descriptor
    # in the open file table, pointing to the corresponding
    # inode. file descriptor is just an handle used to find the
    # corresponding inode. This handle is allocated by the filesystem.
    def open(self, path):
        inode_index = self.fs.namei(path)
        inode = self.fs.inodes_list[inode_index]
        self.openfiles.append(inode)
        return len(self.openfiles) - 1

    # release file descriptor entry, should we flush buffers : no, this is separate ?
    def close(self, fd):
        self.openfiles[fd] = None

    # read nbytes from the inode, starting at the given offset
    def __read(self, inode, offset, count):
        # todo: check inode size limit
        start_block = math.trunc(offset / self.fs.blockdevice.blocksize)
        end_block = math.trunc((offset + count) / self.fs.blockdevice.blocksize)
        blocks_data = ""
        for i in range(start_block, end_block + 1):
            blk_num = self.fs.bmap(inode, i)
            blocks_data += self.fs.blockdevice.blocksize * '\0' if blk_num == 0 else self.fs.blockdevice.read_bloc(blk_num)

        block_offset = offset % self.fs.blockdevice.blocksize
        return blocks_data[block_offset: block_offset + count]

    # read nbytes from the file descriptor previously opened, starting at the given offset
    def read(self, fd, offset, count):
        return self.__read(self.openfiles[fd], offset, count)

    # get the attributes of a node in a stat dictionnary :
    # keys st_ctime, st_mtime, st_nlink, st_mode, st_size, st_gid, st_uid, st_atime
    #{'st_ctime': 1419027551.4907832, 'st_mtime': 1419027551.4907832, \
    # 'st_nlink': 36, 'st_mode': 16877, 'st_size': 4096, 'st_gid': 0, \
    #  'st_uid': 0, 'st_atime': 1423220038.6543322}
    def attr(self, path):
        inode_index = self.fs.namei(path)
        inode = self.fs.inodes_list[inode_index]

        return {
            'st_ctime': inode.i_ctime,
            'st_mtime': inode.i_mtime,
            'st_blksize': self.fs.blockdevice.blocksize,
            'st_blocks': int(inode.i_size / self.BLOCK_DATA_SIZE) + 1,
            'st_nlink': inode.i_links_count,
            'st_mode': inode.i_mode,
            'st_size': inode.i_size,
            'st_gid': inode.i_gid,
            'st_uid': inode.i_uid,
            'st_atime': inode.i_atime
        }

    # implementation of readdir(3) :
    # open the named file, and read each dir_entry in it
    # note that is not a syscall but a function from the libc
    def dodir(self, path):
        inode_index = self.fs.namei(path)
        inode = self.fs.inodes_list[inode_index]

        return [e[1] for e in self.fs.dir_entries(inode)]
