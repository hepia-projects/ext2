# -*- coding: utf-8 -*-
# emulate a simple block device using a file
# reading it only by block units

class bloc_device(object):
    blockdevice = None
    blocksize = 0

    # Open the designated file as a block device
    def __init__(self, blksize, pathname):
        # For some reason, unit test pass a float for 'blksize' which cause errors
        self.blocksize = int(blksize)
        self.blockdevice = open(pathname, "rb")

    # Close the device on destruction
    def __del__(self):
        self.blockdevice.close()
        self.blockdevice = None

    def read_bloc(self, block_num, numofblk=1):
        self.blockdevice.seek(self.blocksize * block_num)
        return self.blockdevice.read(self.blocksize * numofblk)
