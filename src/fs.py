# -*- coding: utf-8 -*-

from math import pow, ceil
from bloc_device import *
import fs_inode
import fs_superbloc
import fs_bloc_group
import bitarray
import struct

# This class implements read only ext2 filesystem

class ext2(object):
    BASEBLOCKSIZE = 1024
    BLOCKGROUP_SIZE_BYTE = 32
    BLOCKNUM_SIZE_BYTE = 4
    NUM_BLOCK_SIMPLE = 12
    NUM_INDIRECTION_LEVEL = 3
    ENTRY_INODE_SIZE_BYTE = 4
    ENTRY_LEN_OFFSET = ENTRY_INODE_SIZE_BYTE
    ENTRY_LEN_SIZE_BYTE = 2
    ENTRY_NAMELEN_OFFSET = ENTRY_LEN_OFFSET + ENTRY_LEN_SIZE_BYTE
    ENTRY_NAMELEN_SIZE_BYTE = 1
    ENTRY_NAME_OFFSET = ENTRY_NAMELEN_OFFSET + ENTRY_NAMELEN_SIZE_BYTE + 1  # 1 byte for file type
    DIR_ROOT_INODE_NUM = 2
    DIR_SEPARATOR = "/"

    superbloc = None
    blockdevice = None
    bgroup_desc_list = None
    bloc_map = None
    inode_map = None
    inodes_list = None

    # General structure: http://www.nongnu.org/ext2-doc/ext2.html#DISK-ORGANISATION
    def __init__(self, filename):
        self.superbloc = fs_superbloc.ext2_superbloc(filename)
        block_size = self.BASEBLOCKSIZE << self.superbloc.s_log_block_size
        self.blockdevice = bloc_device(block_size, filename)

        # http://www.nongnu.org/ext2-doc/ext2.html#DEF-BLOCK-GROUPS
        # The block table is the "third block on a 1KiB block file system, or the second block for 2KiB and larger block file systems"
        self.bgroup_desc_list = []
        bgroup_table = self.blockdevice.read_bloc(1 if self.superbloc.s_log_block_size else 2, 1)

        # The number of elements in the table is determined by how many times with can put block groups in the entire disk
        # The div is not round, so ceil result to have all groups
        bgroup_table_nb = int(ceil(float(self.superbloc.s_blocks_count) / self.superbloc.s_blocks_per_group))

        self.bgroup_desc_list = [fs_bloc_group.ext2_bgroup_desc(bgroup_table[i * self.BLOCKGROUP_SIZE_BYTE:(i + 1) * self.BLOCKGROUP_SIZE_BYTE])
                                 for i in xrange(bgroup_table_nb)]

        # http://www.nongnu.org/ext2-doc/ext2.html#BLOCK-GROUP-DESCRIPTOR-TABLE
        # As per TP doc, only the first block maps are loaded
        # Bitmap position is described in the blockgroup data
        if self.bgroup_desc_list:
            bblock_map = self.blockdevice.read_bloc(self.bgroup_desc_list[0].bg_block_bitmap)
            self.bloc_map = bitarray.bitarray(endian='little')
            self.bloc_map.frombytes(bblock_map)
            binode_map = self.blockdevice.read_bloc(self.bgroup_desc_list[0].bg_inode_bitmap)
            self.inode_map = bitarray.bitarray(endian='little')
            self.inode_map.frombytes(binode_map)

        # http://www.nongnu.org/ext2-doc/ext2.html#INODE-TABLE
        self.inodes_list = []
        # Inode 0 doesn't exist on disk, but tests need it
        self.inodes_list.append(fs_inode.ext2_inode(None))
        # Number of blocks for group inode table
        nb_blocks_inodetable = int(ceil(float(self.superbloc.s_inodes_per_group * self.superbloc.s_inode_size) / self.blockdevice.blocksize))

        for bg in self.bgroup_desc_list:
            inode_table = self.blockdevice.read_bloc(bg.bg_inode_table, nb_blocks_inodetable)
            # generate ext2_inode from inode data read from inode table
            self.inodes_list.extend([fs_inode.ext2_inode(inode_table[i_inode * self.superbloc.s_inode_size:(i_inode + 1) * self.superbloc.s_inode_size], i_inode + 1)
                                     for i_inode, inode in enumerate(inode_table[::self.superbloc.s_inode_size])])

    # find the directory inode number of a path
    # given : ex '/usr/bin/cat' return the inode
    # of '/usr/bin'
    def dirnamei(self, path):
        inode_num = self.DIR_ROOT_INODE_NUM
        path_dirs = [dirname for dirname in path.split(self.DIR_SEPARATOR)[:-1] if dirname is not None]

        for dirname in path_dirs:
            inode = self.inodes_list[inode_num]
            inode_num = self.lookup_entry(inode, dirname)

        return inode_num

    # find an inode number according to its path
    # ex : '/usr/bin/cat'
    # only works with absolute paths
    def namei(self, path):
        dir_inode_num = self.dirnamei(path)
        filename = path.split(self.DIR_SEPARATOR)[-1]

        return self.lookup_entry(self.inodes_list[dir_inode_num], filename) if (filename is not None) else dir_inode_num

    # Make correspondance between a file block and a disk block
    # Return the block ID (disk perspective) from a file block number
    # http://www.nongnu.org/ext2-doc/ext2.html#I-BLOCK
    def bmap(self, inode, blk):
        # Number of block ID in each data block
        nb_blockid_per_block = self.blockdevice.blocksize / self.BLOCKNUM_SIZE_BYTE

        # Determine starting block_id for each level of indirection
        starting_blockid = [0, self.NUM_BLOCK_SIMPLE]
        for i in xrange(1, self.NUM_INDIRECTION_LEVEL + 1):
            starting_blockid.append(starting_blockid[i] + int(pow(nb_blockid_per_block, i)))

        # Determine indirection level by comparing each range of number
        indirection_level = 0
        for i in xrange(0, len(starting_blockid) - 1):
            if (blk >= starting_blockid[i]) and (blk < starting_blockid[i + 1]):
                indirection_level = i
                break

        # Get starting block id from the inode block list
        # Either is simple and it is direct,
        # or it is an indirection and we take last simple block + indirecion level found
        block_id = inode.i_blocks[blk if (indirection_level == 0) else (self.NUM_BLOCK_SIMPLE + indirection_level - 1)]

        # Correction to begin the number from 0 relative to the indirection level's last number
        blk -= starting_blockid[indirection_level]
        # Find the block ID following the number of indirection
        for ind_lvl in xrange(1, indirection_level + 1):
            if block_id == 0:
                return 0

            block_indirect = self.blockdevice.read_bloc(block_id)

            if indirection_level != ind_lvl:
                stage_blks = int(pow(nb_blockid_per_block, indirection_level - ind_lvl))
                current_blk = blk / stage_blks
                blk %= stage_blks
            else:
                current_blk = blk

            blocknum_index = current_blk * self.BLOCKNUM_SIZE_BYTE
            block_id = struct.unpack("<I", block_indirect[blocknum_index:blocknum_index + self.BLOCKNUM_SIZE_BYTE])[0]

        return block_id

    # lookup for a name in a directory, and return its inode number,
    # given inode directory dinode
    def lookup_entry(self, dinode, name):
        for e in self.dir_entries(dinode, name):
            return e[0]
        return 0

    # ext2 store directories in a linked list of records
    # pointing to the next by length
    # - records cannot span multiple blocs.
    # - the end of the linked list as an inode num equal to zero.
    def dir_entries(self, dinode, filterName=None):
        nb_block = dinode.i_size / self.blockdevice.blocksize

        # http://www.nongnu.org/ext2-doc/ext2.html#LINKED-DIRECTORIES
        # For each block of directory listing
        for i in xrange(0, nb_block):
            entry_offset = 0
            block = self.blockdevice.read_bloc(self.bmap(dinode, i))

            # Since entry has a variable size,
            # we use an offset increment to keep track of each entry beginning
            while (entry_offset < self.blockdevice.blocksize):
                entry_size = struct.unpack("<H", block[entry_offset + self.ENTRY_LEN_OFFSET:entry_offset + self.ENTRY_LEN_OFFSET + self.ENTRY_LEN_SIZE_BYTE])[0]
                entry_name_size = struct.unpack("<B", block[entry_offset + self.ENTRY_NAMELEN_OFFSET:entry_offset +
                                                            self.ENTRY_NAMELEN_OFFSET + self.ENTRY_NAMELEN_SIZE_BYTE])[0]
                entry_name = block[entry_offset + self.ENTRY_NAME_OFFSET:entry_offset + self.ENTRY_NAME_OFFSET + entry_name_size]

                if not filterName or entry_name == filterName:
                    inode = struct.unpack("<I", block[entry_offset:entry_offset + self.ENTRY_INODE_SIZE_BYTE])[0]
                    yield (inode, entry_name)

                entry_offset += entry_size
